# Setup CI/CD pipeline on GitLab

This project comes with a simple setup where already CI/CD pipeline is defined. This means every change made to project 
that is pushed to the remote repository will trigger a execution of the pipeline.

For more information on configuring GitLab CI can be found [here](https://docs.gitlab.com/ee/ci/yaml/)

## Prerequisites

The workshop is focused on the minimal usage of tools and is intended for participants to only use their browser.

## Setup GitLab CI/CD variables

To connect to the actual application a variable must be set in the environment of the pipeline. Follow these steps for 
setting the variables:

1. Go to pipeline configuration page use this URL https://gitlab.com/**gitlab-account**/cucumber-workshop/settings/ci_cd 
2. Go to the secret variables and click on **Expand**, this will open the input menu. ![Secret variables](images/secrets.png)
3. Create a new secret variable called **TRIANGLE_URL**, with a value provided in the workshop. ![Secret input](images/secret-input.png)
4. Add the variable by **Add variables**

## .gitlab-ci.yml

During the exercises the **.gitlab-ci.yml** file is extended with additional jobs and stages. **Stages** within the 
context of gitlab are the logical separations between promotions of software to an environment. These stages contain one 
or more **jobs**, these jobs are units of work within a stage and are the quality gates for the software.

### Structure

The [.gitlab-ci.yml](../.gitlab-ci.yml) file is structured in three parts:

- Build settings like default, caching settings and variables
- Stages, the build stages of the CI/CD pipeline
- Jobs, the build steps of the CI/CD pipeline

```yaml
# Default docker image
image: node:10

# GitLab CI settings
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - node_modules/

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

before_script:
  - npm install

# Stages

stages:
  - sample


# Jobs

sample:
  stage: sample
  script:
   - echo 'sample'
```

In the following exercises this file will be extended with additional steps for that will build trust in deployment.