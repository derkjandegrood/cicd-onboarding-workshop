# Smoke testing

Smoke testing is a fail-fast validation if the application is deployed correctly.

## Goals

By adding smoke tests to a continuous delivery pipeline a fail-fast mechanisme is added to test whether and environment 
is deployed correctly. If not these tests should fail.

## Approach

A smoke test is very simple a for an webapplication this can be easily done with curl.

## Exercise

In this exercise a smoke test job and stage are created that validate the endpoint of the application with curl.

### Step 1: Smoke test

Create a smoke_test job and a smoke stage in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test
  - uat
  - deploy
  - smoke

# The smoke test job
smoke_test:
  stage: smoke
  script:
  - curl -X POST $TRIANGLE_URL -d @smoke-test.json
```

The stage **smoke** is an new logical divider of steps within the continuous delivery pipeline. The job **smoke_test** is 
actual command that starts the smoke test. 
