const application = require('./../src/index.js');


test('Triangle with 3 equal sides', () => {

    callback = (error, returnObject) => {
        expect(error).toBe(null);
        expect(returnObject.triangleType).toBe("equilateral");
    };

    let event = {"side1": 1, "side2": 1, "side3": 1};
    let context = {};
    application.handler(event, context, callback);
});

test('Triangle with 2 equal sides', () => {

    callback = (error, returnObject) => {
        expect(error).toBe(null);
        expect(returnObject.triangleType).toBe("isosceles");
    };

    let event = {"side1": 1, "side2": 1, "side3": 2};
    let context = {};
    application.handler(event, context, callback);
});

test('Triangle with no equal sides', () => {

    callback = (error, returnObject) => {
        expect(error).toBe(null);
        expect(returnObject.triangleType).toBe("scalene");
    };

    let event = {"side1": 1, "side2": 2, "side3": 3};
    let context = {};
    application.handler(event, context, callback);
});

test('Triangle with invalid input', () => {

    callback = (error, returnObject) => {
        expect(error).toEqual(new Error("invalid input"));
    };

    let event = {"side1": 1, "side2": "invalid", "side3": 3};
    let context = {};
    application.handler(event, context, callback);
});

